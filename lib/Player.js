function Player(tile, bulletTile) {
  Element.call(this, tile, {
    x : 0,
    y : 0.7,
    hitBoxRadius : 0.01,
    radius: 0.01
  });
  this.bulletTile = bulletTile;
  this.speed = 1;
  this.focusSpeed = 0.25;
  this.LastShoot = 0;
  this.alive = true;
}

Player.prototype = Object.create(Element.prototype);

Player.prototype.step = function(keys, time, time_abs, bullets) {
  var pos = this.pos;
  var moveX = 0;
  var moveY = 0;
  if (keys.up) {
    moveY -= 1;
  }
  if (keys.down) {
    moveY += 1;
  }
  if (keys.left) {
    moveX -= 1;
  }
  if (keys.right) {
    moveX += 1;
  }
  if (moveY && moveX) {
    var t = 1 / Math.sqrt(2);
    moveX *= t;
    moveY *= t;
  }
  pos.x += moveX * time * (keys.focus ? this.focusSpeed : this.speed) / 1000;
  pos.y += moveY * time * (keys.focus ? this.focusSpeed : this.speed) / 1000;

  if (pos.x < -0.95) {
    pos.x = -0.95;
  }
  if (pos.y < -0.95) {
    pos.y = -0.95;
  }
  if (pos.x > 0.95) {
    pos.x = 0.95;
  }
  if (pos.y > 0.95) {
    pos.y = 0.95;
  }

  if (keys.shoot && time_abs - this.LastShoot > 100) {
    bullets.push(new Bullet(this.bulletTile, {
      x : this.pos.x,
      y : this.pos.y - 0.02,
      vX: 0,
      vY: -2,
      hitBoxRadius : 0.01,
      radius : 0.01
    }));
    this.LastShoot = time_abs;
  }
}

Player.prototype.damage = function() {
  this.alive = false;
}

