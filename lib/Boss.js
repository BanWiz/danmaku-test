function Boss(tile, bulletTile, pos) {
  Element.call(this, tile, pos || {
    x : 0,
    y : -0.7,
    radius : 0.2
  });
  this.alive = true;
  this.bulletTile = bulletTile;
  this.lastShot = 0;
  this.angle = 0;
  this.hp = 100;
}

Boss.prototype = Object.create(Element.prototype);

Boss.prototype.damage = function(value) {
  this.hp -= value;
  if(this.hp <= 0) {
    this.alive = false;
  }
}

Boss.prototype.step = function(time, time_abs, enemyBullets, player) {
  if (!this.lastShot) {
    this.lastShot = time_abs;
  }

  var bulletNumber = 10;
  var shootingPeriod = 150;
  var angleStep = 0.1;
  while (this.lastShot < time_abs - shootingPeriod) {
    this.lastShot += shootingPeriod;
    var angle = this.angle;
    this.angle += angleStep;
    for (var i = 0; i < bulletNumber; ++i) {
      var cX = Math.sin(angle);
      var cY = Math.cos(angle);
      var bullet = new Bullet(this.bulletTile, {
        x : this.pos.x,
        y : this.pos.y,
        radius : 0.01,
        vX : cX * 0.2,
        vY : cY * 0.2
      }, 1);
      if (bullet.move(time_abs - this.lastShot, time_abs))
        enemyBullets.push(bullet);
      angle += Math.PI * 2 / bulletNumber;
    }
  }
}