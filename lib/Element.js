function Element(tile, pos) {
  this.pos = pos || {
    x : 0,
    y : 0,
    radius : 0.01
  };
  this.tile = tile;
}

Element.prototype.setPos = function(val) {
  this.pos = val;
}

Element.prototype.render = function(context, canvas) {
  context.drawImage(this.tile, this.pos.x - this.pos.radius, this.pos.y - this.pos.radius, this.pos.radius * 2, this.pos.radius * 2);
}

