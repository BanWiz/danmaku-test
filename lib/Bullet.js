function Bullet(tile, pos, damage) {
  Element.call(this, tile, pos);
  this.pos.vX = pos.vX || 0;
  this.pos.vY = pos.vY || 0;
  this.damage = damage || 1;
}

Bullet.prototype = Object.create(Element.prototype);

Bullet.prototype.move = function(time, time_abs) {
  this.pos.x += this.pos.vX * time / 1000;
  this.pos.y += this.pos.vY * time / 1000;
  return Math.abs(this.pos.x) < 1 + this.pos.radius && Math.abs(this.pos.y) < 1 + this.pos.radius;
}

Bullet.prototype.collide = function(targets) {
  var len = targets.length;
  for (var i = 0; i < len; ++i) {
    if (targets[i].alive && Math.sqrt(Math.pow(targets[i].pos.x - this.pos.x, 2) + Math.pow(targets[i].pos.y - this.pos.y, 2)) < targets[i].pos.radius + this.pos.radius) {
      targets[i].damage(this.damage);
      return false;
    }
  }
  return true;
}