function VerticalBullet(tile, team, pos, speed) {
  Element.call(this, tile, 1, {
    x : 0,
    y : 0.7,
    width : 0.02,
    height : 0.02
  });
}

VerticalBullet.prototype = Object.create(Element.prototype);

addEventListener('load', function() {
  function fullScreen(element) {
    if (element.requestFullscreen) {
      element.requestFullscreen();
    } else if (element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    } else if (element.mozRequestFullscreen) {
      element.mozRequestFullScreen();
    }
  }

  var canvas = document.createElement('canvas');
  canvas.width = 1024;
  canvas.height = 768;
  document.body.appendChild(canvas);

  var context = canvas.getContext('2d');

  var keyListeners = {
    16 : "focus",
    90 : "shoot",
    88 : "bomb",
    37 : "left",
    38 : "up",
    39 : "right",
    40 : "down"
  };
  var keyPresses = {};

  var tiles = {
    enemy : "enemy.png",
    bullet : "bullet.png",
    player : "player.png",
    playerBullet : "player_bullet.png"
  }

  for (var key in tiles) {
    if (tiles.hasOwnProperty(key)) {
      var t = new Image();
      t.src = 'images/' + tiles[key];
      tiles[key] = t;
    }
  }

  var player = new Player(tiles.player, tiles.playerBullet), playerBullets = [], enemies = [], enemyBullets = [];

  enemies.push(new Boss(tiles.enemy, tiles.bullet, {
    x : 0.5,
    y : -0.7,
    radius : 0.2
  }));
  
  enemies.push(new Boss(tiles.enemy, tiles.bullet, {
    x : -0.5,
    y : -0.7,
    radius : 0.2
  }));

  function redraw(time, time_abs) {
    canvas.width = document.documentElement.clientWidth;
    canvas.height = document.documentElement.clientHeight;
    context.setTransform(canvas.height / 2, 0, 0, canvas.height / 2, canvas.width / 2, canvas.height / 2);
    context.fillStyle = '#eeeeee';
    context.fillRect(-1, -1, 2, 2);
    if (player.alive) {
      player.step(keyPresses, time, time_abs, playerBullets, enemies);
    }

    for (var i = 0; i < playerBullets.length; ++i) {
      if (playerBullets[i].move(time, time_abs) && playerBullets[i].collide(enemies)) {
        playerBullets[i].render(context, canvas);
      } else {
        playerBullets = playerBullets.slice(0, i).concat(playerBullets.slice(i-- + 1, playerBullets.length));
      }
    }

    if (player && player.alive) {
      player.render(context, canvas);
    }

    for (var i = 0; i < enemyBullets.length; ++i) {
      if (enemyBullets[i].move(time, time_abs) && enemyBullets[i].collide([player])) {
        enemyBullets[i].render(context, canvas);
      } else {
        enemyBullets = enemyBullets.slice(0, i).concat(enemyBullets.slice(i-- + 1, enemyBullets.length));
      }
    }
    
    for (var i = 0; i < enemies.length; ++i) {
      enemies[i].step(time, time_abs, enemyBullets, player);
      if (enemies[i].alive) {
        enemies[i].render(context, canvas);
      } else {
        enemies = enemies.slice(0, i).concat(enemies.slice(i-- + 1, enemies.length));
      }
    }
    
    context.fillStyle = "#ffffff";
    context.fillRect(-100, -1, 99, 2);
    context.fillRect(1, -1, 99, 2);
  }

  addEventListener('keydown', function(e) {
    if (keyListeners[e.keyCode]) {
      keyPresses[keyListeners[e.keyCode]] = true;
    }
    if (e.keyCode == 70) {
      fullScreen(canvas);
    }
  });

  addEventListener('keyup', function(e) {
    if (keyListeners[e.keyCode]) {
      keyPresses[keyListeners[e.keyCode]] = false;
    }
  });

  var loop;
  (function() {
    var lastAppear;
    loop = function(t) {
      redraw( lastAppear ? t - lastAppear : 0, t);
      lastAppear = t;
      requestAnimationFrame(loop);
    };
  })();

  loop();
}); 